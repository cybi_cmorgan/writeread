/**
 * @file
 */
//
//  Copyright (c) 2013 Cybex International, Inc. All Rights Reserved
//
/************************************************************************/

#ifndef SERIAL_BAUD_H
#define SERIAL_BAUD_H

#include <termios.h> /* POSIX terminal control definitions */
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

int getbaud( int fd );

/* 
    ripped from 
    http://git.savannah.gnu.org/cgit/coreutils.git/tree/src/stty.c
*/
/** Converts text, like "115200" to a speed_t entry */
bool string_to_baud (const char *arg, speed_t *pResult);

#ifdef __cplusplus
}
#endif

#endif
