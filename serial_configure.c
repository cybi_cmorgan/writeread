/**
 * @file
 */
//
//  Copyright (c) 2013 Cybex International, Inc. All Rights Reserved
//
/************************************************************************/

#include <string.h>
#include "serial_configure.h"

ConfigureSerialResults configureSerialFd(int fd, speed_t baudRate)
{
    struct termios options;

    memset(&options, 0, sizeof(options));

    /* ADDED - else 'read' will not return, unless it sees LF '\n' !!!!
    * From: Unix Programming Frequently Asked Questions - 3. Terminal I/O -
    * http://www.steve.org.uk/Reference/Unix/faq_4.html
    */
    /* Set buffer size to 1 byte */
    options.c_cc[VMIN] = 1;

    // Set the baud rates to...
    if(cfsetispeed( &options, baudRate ))
    {
        return ConfigureSerialFailedToSetISpeed;
    }

    if(cfsetospeed( &options, baudRate ))
    {
        return ConfigureSerialFailedToSetOSpeed;
    }

    // Enable the receiver and set local mode and 8 data bits
    options.c_cflag |= ( CLOCAL | CREAD );
    options.c_cflag |= CS8;

    // Flush the input & output...
    tcflush( fd, TCIOFLUSH );

    // Set the new options for the port...
    tcsetattr( fd, TCSANOW, &options );

    return ConfigureSerialSuccess;
}
