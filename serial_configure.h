/**
 * @file
 */
//
//  Copyright (c) 2013 Cybex International, Inc. All Rights Reserved
//
/************************************************************************/

#ifndef SERIAL_CONFIGURE_H
#define SERIAL_CONFIGURE_H

#include <termios.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum ConfigureSerialResults
{
    ConfigureSerialSuccess,
    ConfigureSerialFailedToSetISpeed,
    ConfigureSerialFailedToSetOSpeed
} ConfigureSerialResults;

/**
 * Configure a serial fd for a given baud rate
 */
ConfigureSerialResults configureSerialFd(int fd, speed_t baudRate);

#ifdef __cplusplus
}
#endif

#endif

