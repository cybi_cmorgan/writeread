Required packages:

- Boost program options
   Ubuntu: 'sudo apt-get install libboost-program-options-dev'


How to build
==============
We use out-of-source building so you'll want to do something like:

	mkdir build
	cd build
	cmake ../
