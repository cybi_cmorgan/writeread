/*
    writeread.c - based on writeread.cpp
    [SOLVED] Serial Programming, Write-Read Issue - http://www.linuxquestions.org/questions/programming-9/serial-programming-write-read-issue-822980/

    build with: gcc -o writeread -lpthread -Wall -g writeread.c

    Presumes that the device is enabled for loopback, either that the device has been wired for loopback
    or that the device software will perform the loopback

    A comparison between the data written and the data read is performed at the end of the
    test
*/

#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdarg.h>

#include <stdlib.h>
#include <sys/time.h>

#include <pthread.h>

#include <sys/epoll.h>

#include "serial.h"
#include "serial_baud.h"

#include "ParseOptions.h"

// Define to enable the output of the data file to stdout
//#define OUTPUT_DATA_TO_STDOUT

// time between printing progress updates on stdout
const int timePrintIntervalSeconds = 5;

// Define to enable the output of the data read to a file
#define OUTPUT_DATA_READ_TO_FILE

int serport_fd;

//POSIX Threads Programming - https://computing.llnl.gov/tutorials/pthreads/#PassingArguments
struct write_thread_data{
   int  fd;
   void* pOutputData; // data
   int bytesToSend;
   int writtenBytes;
};

/** Set from command line optons */
LogOutputLevels logOutputLevel;

void logWrite(const char *functionName,
              unsigned int lineNumber,
              enum LogOutputLevels statementLogOutputLevel,
              const char *format, ...)
{
    if(logOutputLevel < statementLogOutputLevel)
    {
        return;
    }

    // print the context info
    printf("%s():%d ", functionName, lineNumber);

    va_list ap;
    va_start(ap, format); /* Initialize the va_list */

    // print the output line
    vprintf(format, ap);

    va_end(ap); /* Cleanup the va_list */

    printf("\n");
}

#define LOG_TRACE(...) \
    logWrite(__FUNCTION__, __LINE__, LogOutputLevelTrace, __VA_ARGS__);

#define LOG_DEBUG(...) \
    logWrite(__FUNCTION__, __LINE__, LogOutputLevelDebug, __VA_ARGS__);

#define LOG_INFO(...) \
    logWrite(__FUNCTION__, __LINE__, LogOutputLevelInfo, __VA_ARGS__);

#define LOG_ERROR(...) \
    logWrite(__FUNCTION__, __LINE__,  LogOutputLevelError, __VA_ARGS__);


#define WRITE_RETRIES 10
int writeErrors = 0;

const int microsecondsInAMillisecond = 1000;

// POSIX threads explained - http://www.ibm.com/developerworks/library/l-posix1.html
void *write_thread_function(void *arg) {
    const int writeRetryDelayMilliseconds = 1000;
    int lastBytesWritten;
    struct write_thread_data *my_data;
    my_data = (struct write_thread_data *) arg;
    int writeRetries = WRITE_RETRIES;
    struct timeval lastPrintTime;

    fprintf(stdout, "write_thread_function spawned\n");

    gettimeofday(&lastPrintTime, NULL);
    lastPrintTime.tv_sec -= timePrintIntervalSeconds; // ensure immediate printing on the first run

    my_data->writtenBytes = 0;
    while(my_data->writtenBytes < my_data->bytesToSend)
    {
        writeRetries--;
        lastBytesWritten = write( my_data->fd, ((char*)(my_data->pOutputData)) + my_data->writtenBytes, my_data->bytesToSend - my_data->writtenBytes );
        LOG_DEBUG("lastBytesWritten %d", lastBytesWritten);

        if ( lastBytesWritten < 0 )
        {
            if((errno == EINTR) || (errno == EAGAIN))
            {
                // signal received, no data transferred
            } else
            {
                writeErrors++;
                fprintf(stdout, "write failed, errno %d, %s, retries %d!\n", errno , strerror(errno), writeRetries);
                if(writeRetries <= 0)
                {
                    fprintf(stdout, "ran out of retries, aborting the write thread\n");
                    exit(1);
                }

                usleep(microsecondsInAMillisecond * writeRetryDelayMilliseconds);
            }
        } else
        {
            my_data->writtenBytes += lastBytesWritten;
            writeRetries = WRITE_RETRIES;
        }

        struct timeval currentTime;
        gettimeofday(&currentTime, NULL);
        struct timeval elapsedTime;
        timeval_subtract(&elapsedTime, &currentTime, &lastPrintTime);

        if(elapsedTime.tv_sec >= timePrintIntervalSeconds)
        {
            fprintf(stderr, "   write: %d - %d\n", lastBytesWritten, my_data->writtenBytes);
            gettimeofday(&lastPrintTime, NULL);
        }
    }
    return NULL; //pthread_exit(NULL)
}

enum ComparisonResults
{
    ComparisonResultMatch,
    ComparisonResultLengthMismatch,
    ComparisonResultDataMismatch
};

enum ComparisonResults CompareResponseToInput(const void* response, int responseBytes, const void *input, int inputBytes)
{
    if(responseBytes != inputBytes)
    {
        return ComparisonResultLengthMismatch;
    }

    if(memcmp(response, input, inputBytes) == 0)
    {
        return ComparisonResultMatch;
    } else
    {
        return ComparisonResultDataMismatch;
    }
}

typedef struct TestResults_s
{
    struct timeval timeStart;
    struct timeval timeEnd;
    enum ComparisonResults dataComparisonResult;
    int writtenBytes;
    int receivedBytes;
} TestResults;

bool RunTest(int writeFd, int readFd,
             void *pOutputData,
             int bytesToSend,
             TestResults &result,
             bool shouldOutputDataToFile,
             const char *outputFilename)
{
    memset(&result, 0, sizeof(result));

    int readChars;
    int recdBytes;
    struct write_thread_data wrdata;
    pthread_t myWriteThread;
    struct timeval lastPrintTime;

    char* sResp;
    char* sRespTotal;

    sResp = (char *)calloc(bytesToSend, sizeof(char));
    sRespTotal = (char *)calloc(bytesToSend, sizeof(char));

    wrdata.fd = writeFd;
    wrdata.bytesToSend = bytesToSend;
    wrdata.pOutputData = pOutputData;

    recdBytes = 0;

    gettimeofday( &result.timeStart, NULL );
    gettimeofday(&lastPrintTime, NULL);
    lastPrintTime.tv_sec -= timePrintIntervalSeconds; // ensure immediate printing on the first run

    // start the thread for writing..
    if ( pthread_create( &myWriteThread, NULL, write_thread_function, (void *) &wrdata) ) {
        printf("error creating thread.");
        abort();
    }

    /** Array used by epoll() to report events that have occurred */
    const static int MAX_EVENTS = 10;
    struct epoll_event events[MAX_EVENTS];

    // create the epoll instance
    int epollFd = epoll_create1(0);
    if(epollFd == -1)
    {
        printf("epoll_create1() error\n");
        abort();
    }

    epoll_event readEvent = {0};
    readEvent.events = EPOLLIN | EPOLLRDHUP;
    readEvent.data.fd = readFd;

    // add the new connection descriptor to the list of descriptors to poll
    if(epoll_ctl(epollFd, EPOLL_CTL_ADD, readFd, &readEvent) == -1)
    {
        printf("error epoll_ctl\n");
        abort();
    }

    // run read loop
    while ( recdBytes < wrdata.bytesToSend )
    {
        int nfds = epoll_wait(epollFd, events, MAX_EVENTS, -1);

        // process all of the epoll events
        for(int i = 0; i < nfds; i++)
        {
            struct epoll_event *pEvent = events + i;

            if(pEvent->data.fd == readFd)
            {
                readChars = read( readFd, sResp, wrdata.bytesToSend);
                LOG_DEBUG("readChars %d", readChars);

                if ( readChars >= 0 )
                {
                    memmove(sRespTotal+recdBytes, sResp+0, readChars*sizeof(char));
                    recdBytes += readChars;
                } else
                {
                    if ( errno == EAGAIN )
                    {
                        fprintf(stdout, "SERIAL EAGAIN ERROR\n");
                        return false;
                    }
                    else
                    {
                        fprintf(stdout, "SERIAL read error: %d = %s\n", errno , strerror(errno));
                        return false;
                    }
                }

                struct timeval currentTime;
                gettimeofday(&currentTime, NULL);
                struct timeval elapsedTime;
                timeval_subtract(&elapsedTime, &currentTime, &lastPrintTime);

                if(elapsedTime.tv_sec >= timePrintIntervalSeconds)
                {
                    fprintf(stderr, "   read: %d\n", recdBytes);
                    gettimeofday(&lastPrintTime, NULL);
                }
            }
        }
    }

    // wait until the write thread has shut down
    if ( pthread_join ( myWriteThread, NULL ) ) {
        printf("error joining thread.");
        abort();
    }

    gettimeofday( &result.timeEnd, NULL );

    // compare what was read with what was written
    result.dataComparisonResult = CompareResponseToInput(sRespTotal, recdBytes, wrdata.pOutputData, wrdata.writtenBytes);

    if(shouldOutputDataToFile)
    {
        fprintf(stdout, "Writing data read to '%s'\n", outputFilename);
        FILE* fpOut = fopen(outputFilename, "wb");
        fwrite(sRespTotal, 1, recdBytes, fpOut);
        fclose(fpOut);
    }

    free(sResp);
    free(sRespTotal);

    result.writtenBytes = wrdata.writtenBytes;
    result.receivedBytes = recdBytes;

    return true;
}

int main( int argc, char **argv )
{
    int exit_code = 0;

    program_options options;
    bool parseResult = ParseOptions(argc, argv, options);

    // if we failed to parse options or if there were missing options then
    // we should return an error code and not proceed
    if(!parseResult)
    {
        exit_code = EXIT_CODE_FOR_INVALID_PARSE_OPTIONS;
        return exit_code;
    }

    logOutputLevel = options.logOutputLevel;

    const char *serport;
    const char *serspeed;
    speed_t serspeed_t;
    const char *serfstr;
    int serf_fd;
    int totlBytes;

    struct timeval timeDelta;
    float deltasec, expectBps, measReadBps, measWriteBps;

#if defined(OUTPUT_DATA_TO_STDOUT)
    /* Re: connecting alternative output stream to terminal -
    * http://coding.derkeiler.com/Archive/C_CPP/comp.lang.c/2009-01/msg01616.html
    * send read output to file descriptor 3 if open,
    * else just send to stdout
    */
    FILE *stdalt;
    if(dup2(3, 3) == -1) {
        fprintf(stdout, "stdalt not opened; ");
        stdalt = fopen("/dev/tty", "w");
    } else {
        fprintf(stdout, "stdalt opened; ");
        stdalt = fdopen(3, "w");
    }
    fprintf(stdout, "Alternative file descriptor: %d\n", fileno(stdalt));
#endif

    // Get the PORT name
    serport = options.serialDevice.c_str();
    fprintf(stdout, "Opening port %s;\n", serport);

    // Get the baudrate
    serspeed = options.baud.c_str();
    bool string_to_baud_result = string_to_baud(serspeed, &serspeed_t);
    if(!string_to_baud_result)
    {
        printf("invalid baud rate\n");
        exit_code = EXIT_CODE_FOR_INVALID_PARSE_OPTIONS;
        return exit_code;
    } else
    {
        fprintf(stdout, "Got speed %s (%d/0x%x);\n", serspeed, serspeed_t, serspeed_t);
    }

    int bytesToSend;
    char *outputData;

    //Get file
    serfstr = options.inputDataFilename.c_str();
    serf_fd = open( serfstr, O_RDONLY );
    fprintf(stdout, "Got file '%s'; ", serfstr);

    struct stat st;
    stat(serfstr, &st);
    bytesToSend = st.st_size;
    outputData = (char *)calloc(bytesToSend, sizeof(char));
    read(serf_fd, outputData, bytesToSend);
    fprintf(stdout, "opened as file (%d).\n", bytesToSend);

    // Open and Initialise port
    serport_fd = open( serport, O_RDWR | O_NOCTTY | O_NONBLOCK );
    if ( serport_fd < 0 ) { perror(serport); return 1; }
    if(!initport( serport_fd, serspeed_t ))
    {
        printf("initport failed\n");
        abort();
    }

    fcntl(serport_fd, F_SETFL, 0); // restore normal (blocking) behavior

    bool shouldOutputDataToFile = false;
    char *outputFileName;

    shouldOutputDataToFile = true;
    const char *outputExtension = ".read";
    int outputFileNameMaxLength = strlen(serfstr) + strlen(outputExtension) + 1;
    outputFileName = (char*)malloc(outputFileNameMaxLength);
    snprintf(outputFileName, outputFileNameMaxLength, "%s%s", serfstr, outputExtension);

    // run the test
    TestResults result;
    bool retval = RunTest(serport_fd, serport_fd,
                          outputData,
                          bytesToSend,
                          result,
                          shouldOutputDataToFile,
                          outputFileName);

    if(!retval)
    {
        printf("RunTest() failed\n");
    }

#if defined(OUTPUT_DATA_TO_STDOUT)
    // binary safe - dump sRespTotal to stdalt
    fwrite(sRespTotal, sizeof(char), recdBytes, stdalt);

    // Close the open port
    close( serport_fd );
    close( serf_fd );
    free(comm);
#endif

    switch(result.dataComparisonResult)
    {
    case ComparisonResultMatch:
        fprintf(stdout, "match\n");
        break;
    case ComparisonResultDataMismatch:
        fprintf(stdout, "data mismatch\n");
        break;
    case ComparisonResultLengthMismatch:
        fprintf(stdout, "length mismatch\n");
        break;
    }

    fprintf(stdout, "\n+++DONE+++\n");

    int sentBytes = result.writtenBytes;
    totlBytes = sentBytes + result.receivedBytes;
    timeval_subtract(&timeDelta, &result.timeEnd, &result.timeStart);
    deltasec = timeDelta.tv_sec+timeDelta.tv_usec*1e-6;
    expectBps = atoi(serspeed)/10.0f;
    measWriteBps = sentBytes/deltasec;
    measReadBps = result.receivedBytes/deltasec;

    // totlBytes is the sum of the sent and received bytes, divide the
    // total bytes in half before calculating the transfer rate
    float transferRateBps = (totlBytes / 2) / deltasec;

    fprintf(stdout, "Wrote: %d bytes; Read: %d bytes; Total: %d bytes. \n", sentBytes, result.receivedBytes, totlBytes);
    fprintf(stdout, "Write errors: %d\n", writeErrors);
    fprintf(stdout, "Start: %ld s %ld us; End: %ld s %ld us; Delta: %ld s %ld us. \n", result.timeStart.tv_sec, result.timeStart.tv_usec, result.timeEnd.tv_sec, result.timeEnd.tv_usec, timeDelta.tv_sec, timeDelta.tv_usec);
    fprintf(stdout, "%s baud for 8N1 is %d Bps (bytes/sec).\n", serspeed, (int)expectBps);
    fprintf(stdout, "Measured: write %.02f Bps (%.02f%%), read %.02f Bps (%.02f%%), total %.02f Bps.\n", measWriteBps, (measWriteBps/expectBps)*100, measReadBps, (measReadBps/expectBps)*100, transferRateBps);

    if(options.setBpsMaximumLimit)
    {
        if(transferRateBps > options.bpsMaximumLimit)
        {
            printf("Transfer rate %fbps exceeded expected maximum of %dbps\n",
                    transferRateBps,
                    options.bpsMaximumLimit);
            exit_code = EXIT_CODE_BPS_HIGHER_THAN_LIMIT;
        }
    }

    if(options.setBpsMinimumLimit)
    {
        if(transferRateBps < options.bpsMinimumLimit)
        {
            printf("Transfer rate %fbps lower than minimum of %dbps\n",
                    transferRateBps,
                    options.bpsMinimumLimit);
            exit_code = EXIT_CODE_BPS_LOWER_THAN_LIMIT;
        }
    }

    return exit_code;
}
