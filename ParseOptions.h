#ifndef PARSEOPTIONS_H_
#define PARSEOPTIONS_H_

#include <string>
using namespace std;

#define EXIT_CODE_FOR_INVALID_PARSE_OPTIONS -1
#define EXIT_CODE_BPS_LOWER_THAN_LIMIT      -2
#define EXIT_CODE_BPS_HIGHER_THAN_LIMIT     -3

enum LogOutputLevels
{
    LogOutputLevelError,
    LogOutputLevelInfo,
    LogOutputLevelDebug,
    LogOutputLevelTrace
};

/** Options set by calling ParseOptions() */
typedef struct program_options_t
{
    string baud;
    string serialDevice;
    string inputDataFilename;

    bool setBpsMaximumLimit;
    int bpsMaximumLimit;

    bool setBpsMinimumLimit;
    int bpsMinimumLimit;

    /**
     * Log output greater than or equal to this level will
     * be output to stdout.
     */
    LogOutputLevels logOutputLevel;
} program_options;

/** Returns true if options appear valid, false otherwise */
bool ParseOptions(int argc, char **argv, program_options &options);

#endif
