#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <iostream>
#include "ParseOptions.h"

namespace po = boost::program_options;

/** Returns true if the conversion from string to level was successful */
bool logLevelFromString(string logLevelString, LogOutputLevels &outputLevel)
{
    bool returnValue = true;

    const char *logLevelCString = logLevelString.c_str();

    if(strcasecmp(logLevelCString, "trace") == 0)
    {
        outputLevel = LogOutputLevelTrace;
    } else if(strcasecmp(logLevelCString, "debug") == 0)
    {
        outputLevel = LogOutputLevelDebug;
    } else if(strcasecmp(logLevelCString, "info") == 0)
    {
        outputLevel = LogOutputLevelInfo;
    } else if(strcasecmp(logLevelCString, "error") == 0)
    {
        outputLevel = LogOutputLevelError;
    } else
    {
        returnValue = false;
    }

    return returnValue;
}

bool ParseOptions(int argc, char **argv, program_options &options)
{
    bool parsing_error = false;
    bool retval = true;

    options.serialDevice = "";

    ostringstream minimumHelp;
    minimumHelp << "Lowest acceptable average throughput bps, exit code of " << EXIT_CODE_BPS_LOWER_THAN_LIMIT << " if average throughput is lower";
    ostringstream maximumHelp;
    maximumHelp << "Highest acceptable average throughput bps, exit code of " << EXIT_CODE_BPS_HIGHER_THAN_LIMIT << " if average throughput is higher";

    // Declare the supported options.
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,?", "Usage information")
        ("baud,b", po::value<string>(), "Baud rate in bps (if serial device is used)")
        ("serdev,s", po::value<string>(), "Serial device name (ex. /dev/serial1)")
        ("file,f", po::value<string>(), "Input data filename")
        ("bps_minimum,l", po::value<int>(), minimumHelp.str().c_str())
        ("bps_maximum,h", po::value<int>(), maximumHelp.str().c_str())
        ("log_level,o", po::value<string>(), "Log output level, one of 'trace', 'debug', 'info', 'error'")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return retval;
    }

    if(vm.count("baud"))
    {
        options.baud = vm["baud"].as<string>();
    }

    if (vm.count("serdev")) {
        options.serialDevice = vm["serdev"].as<string>();
    }

    if(vm.count("file"))
    {
        options.inputDataFilename = vm["file"].as<string>();
    }

    if(vm.count("bps_minimum"))
    {
        options.bpsMinimumLimit = vm["bps_minimum"].as<int>();
        options.setBpsMinimumLimit = true;
    } else
    {
        options.setBpsMinimumLimit = false;
    }

    if(vm.count("bps_maximum"))
    {
        options.bpsMaximumLimit = vm["bps_maximum"].as<int>();
        options.setBpsMaximumLimit = true;
    } else
    {
        options.setBpsMaximumLimit = false;
    }

    if(vm.count("log_level"))
    {
        if(!logLevelFromString(vm["log_level"].as<string>(), options.logOutputLevel))
        {
            cout << "Log level should be one of the given options" << endl;
            parsing_error = true;
        }
    } else
    {
        // default log level
        options.logOutputLevel = LogOutputLevelError;
    }

    // make sure the required parameters are set
    if(!vm.count("baud") || !vm.count("serdev") || !vm.count("file"))
    {
        cout << "Must set 'baud', 'serdev', and 'file'" << endl;
        parsing_error = true;
    }

    if(parsing_error)
    {
        cout << "Parameter error" << endl;
        cout << desc << endl;
        retval = false;
        return retval;
    }

    return retval;
}
